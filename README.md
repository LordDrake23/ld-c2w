#LD-C2W

* Version: 1.0
* [Website](http://teamdrake23.no-ip.org/)
* [Documentation](https://bitbucket.org/LordDrake23/ld-C2W/wiki/Home)

## Description

LD-C2W is aiming to be a fully fledged eBook library with a few interesting features.

## More information

For more detailed information, see the [wiki](https://bitbucket.org/LordDrake/LD-C2W/Wiki/About).

##Development Team

* TheDrake23 - Lead Developer ([Website]((http://devdrake.zapto.org/)

### Want to aid us?
 
We'Re always looking for a hand in making out product even better.

You found a bug? Great, report it on our [issue tracker](https://bitbucket.org/LordDrake/LD-C2W/issues/)

You added a feature to our software and want to share it with other users? Submit a pull request.

You want to join our development team? Great, drop us a message.
