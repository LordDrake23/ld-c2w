<?php

/* index.twig */
class __TwigTemplate_d53c1325dab861cbffd72eda025be082e784c55cabb7f73530e27b4b06088b7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );

        $this->macros = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Home";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<h1>There's no content here. Yet.</h1>
<p>This site is in alpha state. We're currently trying to build a set of eBook features like
<ul>
    <li>Provide HTML and OPDS catalogs on a per-user-basis</li>
    <li>View and edit eBook Metadata</li>
    <li>Review eBooks</li>
    <li>Import a dump of your <a href=\"http://www.calibre-ebook.com/\">Calibre</a> library</li>
    <li>an intuitive admin interface</li>
</ul>
<br />
Please keep in mind, that this site and it's features are under heavy development. Features may change, content may change or disappear as we roll back to an earlier backup of the database. Features may not work as intended, may appear incomplete or broken.<br /><br />In any of these cases, please report to the local <a href=\"/groups/members/6\">admins</a>.</p>

<p>If you want to aid development, visit our <a href=\"https://bitbucket.org/LordDrake23/ld-c2w\">BitBucket</a> repo.</p>
";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 6,  38 => 5,  32 => 3,);
    }
}
