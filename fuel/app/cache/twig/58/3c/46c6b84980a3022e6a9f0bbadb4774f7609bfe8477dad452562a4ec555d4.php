<?php

/* base.twig */
class __TwigTemplate_583c46c6b84980a3022e6a9f0bbadb4774f7609bfe8477dad452562a4ec555d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );

        $this->macros = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo " - LD-Dev</title>

        <!-- Bootstrap core CSS -->
        <link href=\"/assets/css/bootstrap.css\" rel=\"stylesheet\">
        <link href=\"/assets/css/footer.css\" rel=\"stylesheet\">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src=\"/assets/js/html5shiv.js\"></script>
          <script src=\"/assets/js/respond.min.js\"></script>
        <![endif]-->
        <style type=\"text/css\"></style></head>

    <body style=\"\">

        <!-- Wrap all page content here -->
        <div id=\"wrap\">
            <nav class=\"navbar navbar-default\" role=\"navigation\">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"/home/index\">LD-Dev</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
                    <ul class=\"nav navbar-nav navbar-right\">
                        ";
        // line 41
        if (($this->getAttribute((isset($context["login"]) ? $context["login"] : null), "is_logged_in") == false)) {
            // line 42
            echo "                            <li class=\"dropdown\" id=\"login\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Login <b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <form method=\"POST\" action=\"/user/login\">
                                        <input type=\"text\" placeholder=\"username\" id=\"uname\" name=\"uname\">
                                        <input type=\"password\" placeholder=\"Password\" id=\"pword\" name=\"pword\">
                                        <input type=\"checkbox\" name=\"rm\" id=\"rm\" value=\"true\"> Remember me
                                        <input type=\"submit\" class=\"btn btn-info\" name=\"submit\" id=\"submit\" value=\"Submit\">
                                    </form>
                                </ul>
                            </li>
                        ";
        }
        // line 54
        echo "                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
                <!-- Begin page content -->
                <div class=\"container\">
                ";
        // line 59
        if ((!twig_test_empty((isset($context["success"]) ? $context["success"] : null)))) {
            // line 60
            echo "                        <div class=\"alert alert-success alert-dismissable\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                            ";
            // line 62
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["success"]) ? $context["success"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 63
                echo "                                ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "body");
                echo " <br />
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "                        </div>
                ";
        }
        // line 67
        echo "                ";
        if ((!twig_test_empty((isset($context["error"]) ? $context["error"] : null)))) {
            // line 68
            echo "                        <div class=\"alert alert-danger alert-dismissable\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                            ";
            // line 70
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["error"]) ? $context["error"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 71
                echo "                                ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "body");
                echo " <br />
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "                        </div>
                ";
        }
        // line 74
        echo "                
                ";
        // line 75
        if ((!twig_test_empty((isset($context["warning"]) ? $context["warning"] : null)))) {
            // line 76
            echo "                        <div class=\"alert alert-success alert-dismissable\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                            ";
            // line 78
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["warning"]) ? $context["warning"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 79
                echo "                                ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "body");
                echo " <br />
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "                        </div>
                ";
        }
        // line 83
        echo "                ";
        if ((!twig_test_empty((isset($context["info"]) ? $context["info"] : null)))) {
            // line 84
            echo "                        <div class=\"alert alert-success alert-dismissable\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                            ";
            // line 86
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["info"]) ? $context["info"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 87
                echo "                                ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "body");
                echo " <br />
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "                        </div>
                ";
        }
        // line 91
        echo "                ";
        $this->displayBlock('content', $context, $blocks);
        // line 95
        echo "                    </div>
                </div>
                <div id=\"footer\">
                    <div class=\"container\">
                        <p class=\"text-muted credit\"><small> Powered by <a href=\"http://www.fuelphp.com\">FuelPHP</a> ";
        // line 99
        echo $this->env->getExtension('fuel')->fuel_version();
        echo " - &copy; by TheDrake23</small></p>
                    </div>
                </div>


                <!-- Bootstrap core JavaScript
                ================================================== -->
                <!-- Placed at the end of the document so the pages load faster -->
                <script src=\"/assets/js/jq.min.js\"></script>
                <script src=\"/assets/js/bootstrap.min.js\"></script>


            </body>
        </html>";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        echo "404";
    }

    // line 91
    public function block_content($context, array $blocks = array())
    {
        // line 92
        echo "                        <h1>Under construction...</h1>
                        <p>... or maybe this page went on vacation. Or there was no content here in the first place... anyways... yell at your local admin to fix this...</p>
\t\t";
    }

    public function getTemplateName()
    {
        return "base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 92,  222 => 91,  216 => 9,  198 => 99,  192 => 95,  189 => 91,  185 => 89,  176 => 87,  172 => 86,  168 => 84,  165 => 83,  161 => 81,  152 => 79,  148 => 78,  144 => 76,  142 => 75,  139 => 74,  135 => 73,  126 => 71,  122 => 70,  118 => 68,  115 => 67,  111 => 65,  102 => 63,  98 => 62,  94 => 60,  92 => 59,  85 => 54,  71 => 42,  69 => 41,  34 => 9,  24 => 1,);
    }
}
