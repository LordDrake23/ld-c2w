<?php

/* __string_template__2e311a84b6f2190cea9dc9eaea59f5f966c55b04d2e07330860fc78d708b6d16 */
class __TwigTemplate_364516856667c63324a84b024f23cecbbe01254f695cb68349889349d63780c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );

        $this->macros = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "&lt;h2&gt;Login successful!&lt;/h2&gt;&lt;br /&gt;You can now access the site&#039;s full functionality.";
    }

    public function getTemplateName()
    {
        return "__string_template__2e311a84b6f2190cea9dc9eaea59f5f966c55b04d2e07330860fc78d708b6d16";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
