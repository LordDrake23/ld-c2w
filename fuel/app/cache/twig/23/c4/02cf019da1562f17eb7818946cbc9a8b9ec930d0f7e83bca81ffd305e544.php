<?php

/* __string_template__0ed699e3ce20d6f207a61c218488f2d486168d6fbb1b9e0c33cd2341545fd482 */
class __TwigTemplate_23c402cf019da1562f17eb7818946cbc9a8b9ec930d0f7e83bca81ffd305e544 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );

        $this->macros = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "&lt;h2&gt;Logout successful!&lt;/h2&gt;&lt;br /&gt;&lt;p&gt;Thank you, please come again!&lt;/p&gt;";
    }

    public function getTemplateName()
    {
        return "__string_template__0ed699e3ce20d6f207a61c218488f2d486168d6fbb1b9e0c33cd2341545fd482";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
