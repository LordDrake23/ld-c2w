<?php

class Controller_Home extends Base_Guest {
	public function before() {
            parent::before();
        }
        public function action_index() {
            $view = View::forge('home/index.twig',$this->data);
            return Response::forge($view);
	}
        
        public function action_404() {
            return Response::forge(View::forge('base.twig',$this->data));
        }
}