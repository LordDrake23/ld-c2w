<?php

class Controller_User extends Base_Guest {

    public function before() {
        parent::before();
    }

    public function action_login() {
        if (Auth::check()) {
            \Messages::error('<h4>Whoops!</h4><br />You are already logged in!');
            \Messages::redirect('/home/index/');
        } else {
            if (Input::method() === "POST" && Auth::login(Input::post('uname'), Input::post('pword'))) {
                \Messages::success('<h4>Login successful!</h4><br />You can now access the site\'s full functionality.');
            } else {
                \Messages::error('<h4>Username/Password mismatch!</h4><br /> Keep in mind that usernames and passwords are case sensitive');
            }
            \Messages::redirect('/home/index');
        }
    }
    
    public function action_logout() {
        if (Auth::check()) {
            Auth::logout();
            \Messages::success('<h4>Logout successful!</h4><br /><p>Thank you, please come again!</p>');
            
        } else {
            \Messages::error('<h4>Whoops!</h4><br /><p>You\'re not logged in, you silly!');
        }
        \Messages::redirect('/home/index');
    }

}