<?php

class Base_Reg extends Base_Guest {
    
   public function before() {
       if (Auth::check()) {
           parent::before();
       } else {
           \Messages::error('You need to be logged in to view this page.');
           \Messages::redirect('/home/index');
       }
   }
}