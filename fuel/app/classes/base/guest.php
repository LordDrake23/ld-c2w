<?php
 
class Base_Guest extends Controller_Rest {
    public function before() {
        parent::before();
        $date = Date::Forge();
        $this->data = array(
            'date'         => $date->format('%d.%m.%y'),
            'time'         => $date->format('%H:%M')
        );
        if (\Messages::any()) {
            View::set_global('success',\Messages::get('success'),false);
            View::set_global('error',\Messages::get('error'),false);
            View::set_global('info',\Messages::get('info'),false);
            View::set_global('warning',\Messages::get('warning'),false);
        }
        \Messages::reset();
    }
}