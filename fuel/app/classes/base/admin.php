<?php

class Base_Admin extends Base_Guest {

    public function before() {
        if (!Auth::check() || !Auth::has_access('admin.view_acp')) {
            \Messages::error('You lack the access rights to view this page.');
            \Messages::redirect('/home/index');
        } else {
            parent::before();
        }
    }

}